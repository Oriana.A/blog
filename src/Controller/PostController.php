<?php

namespace App\Controller;
use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PostController extends AbstractController
{

  /**
   * @Route("/", name="home_page")
   */
  public function homePage()
  {
    $repo = new PostRepository();
    $homePosts = $repo->findAll();

    return $this->render('home.html.twig', [
      'homePosts' => $homePosts
    ]);
  }

  /**
   * @Route("/post/{id}", name="one_post")
   */
  public function onePost(int $id){
    $repo = new PostRepository();
    $post = $repo->findById($id);
    return $this->render('post.html.twig', [
      'post' => $post
    ]);
  }

    /**
     * @Route ("/new-post", name="new_post")
     */
    public function addPost(Request $request){
      $post = null;
      $title = $request->get("title");
      $author = $request->get("author");
      $content = $request->get("content");
     // $postDate = $request->get("postDate");
      if ($title && $author && $content) {
          $post = new Post($title, $author, $content,'now');
          $repo = new PostRepository();
          $repo->add($post);
          return $this->redirectToRoute('home_page');
      }
     return $this->render('newPost.html.twig',[
         'post' => $post
     ]);
  }

    /**
     * @Route("/post/delete/{id}", name = "delete_post")
     */
    public function deletePost(PostRepository $repo, int $id){
      $repo->delete($id);
      return $this->redirectToRoute('home_page');
  }

  /**
   * @Route("/post/modify/{id}", name = "modify_post")
   */
  public function modifypost(Request $request, PostRepository $repo, int $id){
    $oldPost = $repo->findById($id);
    $title = $request->get("title");
    $author = $request->get("author");
    $content = $request->get("content");
    var_dump($title);
    if ($title && $author && $content) {
      $repo->modify($id,$title, $author, $content);
      return $this->redirectToRoute('home_page');
    }

    return $this->render('modifyPost.html.twig',[
      'oldPost' => $oldPost
    ]);
  }
}

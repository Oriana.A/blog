<?php

namespace App\Entity;

use DateTime;

class Post{
protected $id;
private $title;
private $author;
private $postDate;
private $content;

public function __construct(string $title, string $author,string $content,string $postDate = 'now',int $id = null) {
    $this->id = $id;
    $this->title = $title;
    $this->author = $author;
    $this->content = $content;
    $this->postDate = new DateTime($postDate);
}

public function getTitle(): string{
    return $this->title;
}

public function getAuthor(): string{
   return $this->author;
}

public function getPostDate(): \DateTime{
    return $this->postDate;
}

public function getContent(): string{
    return $this->content;
}

public function getId(): int{
   return $this->id;
}

public function setId(int $id): void{
    $this->id = $id;
}

}

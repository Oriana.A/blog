<?php

namespace App\Repository;

use App\Entity\Post;
use DateTime;

class PostRepository
{

    private $pdo;

    public function __construct()
    {
        $this->pdo = new \PDO(
            "mysql:host=" . $_ENV["DATABASE_HOST"] . ";dbname=" . $_ENV["DATABASE_NAME"],
            $_ENV["DATABASE_USERNAME"],
            $_ENV["DATABASE_PASSWORD"],
            [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]

        );
    }

    /**
     * Méthode qui va aller chercher tous les posts
     * présent dans la base de données et les convertir
     * en instance de la classe Post
     * @return Post[] les posts contenus dans la bdd
     */
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM post');
        $query->execute();
        $results = $query->fetchAll();

        $list = [];
        foreach ($results as $line) {
            $post = $this->sqlToPost($line);
            $list[] = $post;
        }
        return $list;
    }


    public function add(Post $post): void
    {
        $now = new DateTime();
        $query = $this->pdo->prepare('INSERT INTO post (title, author, postDate, content) VALUES (:title,:author,:postDate,:content)');
        $query->bindValue('title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue('author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue('postDate', $now->format('Y/m/d H:i:s'), \PDO::PARAM_STR);
        $query->bindValue('content', $post->getContent(), \PDO::PARAM_STR);

        $query->execute();

        $post->setId(intval($this->pdo->lastInsertId()));
    }

    public function findById(int $id): ?Post
    {
        $query = $this->pdo->prepare('SELECT * FROM post WHERE id=:id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();
        if ($line) {
            return $this->sqlToPost($line);
        }
        return null;
    }

    public function delete(int $id)
    {
        $query = $this->pdo->prepare('DELETE FROM post WHERE id=:id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
    }

    public function modify(int $id, string $title, string $author, string $content)
    {
        $query = $this->pdo->prepare('UPDATE post SET title=:title, author=:author, content=:content WHERE id=:id');
        $query->bindValue(':id', $id);
        $query->bindValue(':title', $title);
        $query->bindValue(':content', $content);
        $query->bindValue(':author', $author);
        $query->execute();
    }

    private function sqlToPost(array $line): Post
    {
        return new Post($line['title'], $line['author'], $line['content'], $line['postDate'], $line['id']);
    }
}

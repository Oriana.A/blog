# Projet Blog PHP

Créer un blog en PHP en utilisant le framework Symfony 4.3 et PDO en une semaine 

Ce projet aura pour but : 

- Utiliser Mariadb / PDO pour les bases de données (pas d'ORM) en se basant sur le diagramme de classe fourni 

  - Créer la base de donnée du projet et la table correspondante à l'entité avec des données de tests dedans sous forme de script qui pourra remettre la bdd en état
  - Créer la classe entité correspondant à la table
  - Créer une classe d'accès au données en utilisant PDO (DAO / Repository)

- Créer un projet Symfony et coder l'application avec le framework 

  - Créer des controllers et des templates

  - Créer une classe Form pour le Post

  - Utiliser bootstrap pour les templates (comme d'hab on veut une appli responsive)

  - Dans les controllers, utiliser les DAO que vous avez fait

    

Fonctionnalités attendues :

- Créer des articles
- Consulter la liste des articles
- Consulter un article spécifique
- Modifier ou supprimer un article

### Maquettes

![](public/maquettes/mobile-first.png)

![](public/maquettes/Wireframe.png)

### Diagrammes

![](public/maquettes/Use Case.png)



![](public/maquettes/Diagramme de classe.png)



### Review

Dans le controller Post, j'ai codé cinq méthodes correspondant  à différentes fonctionnalités du blog.

La premère méthode permet d'afficher sur la page principale tous les posts existants dans la base de données et la seconde permet d'accéder à un post en particulier.

![](public/Capture-du-2019-11-03-17-26-58-ConvertImage.png)



La troisième méthode consiste à ajouter un nouveau post puis à rediriger l'utilisateur sur la page principale une fois l'article envoyé sur la base de donnée. La quatrième méthode permet de supprimer un article et de rediriger elle aussi sur la page principale.

![](public/Capture-du-2019-11-03-17-27-12-ConvertImage.png)



Enfin la cinquième méthode permet de modifier un post. En cliquant sur le bouton modifier, l'utilisateur est renvoyé sur un formulaire contenant toutes les données du post. L'utilisateur n'a plus qu'à modifier les informations qu'il souhaite puis de submit les nouvelles données qui seront mis à jour dans la BDD.

![](public/Capture-du-2019-11-03-17-27-17-ConvertImage.png)



Ce Twig permet d'afficher un post en particulier. Les méthodes supprimer et et modifier sont éxécuter aux paths qui renvoit aux méthodes dans le PostController. Les différentes données de l'article sont appelés grâce {{post. ...}}.

![](public/Capture-du-2019-11-03-17-38-17-ConvertImage.png)



#### More info

Author : @Oriana.A

Date : 02/10/2019 - 18/10/2019
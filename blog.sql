--Crée une table post
CREATE TABLE post( 
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    title VARCHAR(80), 
    author VARCHAR(50), 
    postDate DATETIME,
    content TEXT
);

--Ajoute un jeu de données par défaut dans les tables
INSERT INTO post (title,author,postDate,content) VALUES ('The Losers', 'Oria','2018/02/12 18:40:02',"The Losers est une unité d'élite secrète des forces spéciales américaines menée par Clay (Jeffrey Dean Morgan) et composée de Roque (Idris Elba), Pooch (Columbus Short), Jensen (Chris Evans) et Cougar (Oscar Jaenada). Ils sont envoyés en Bolivie pour une mission de recherche et destruction visant un baron de la drogue. Alors qu'ils désignent une cible pour une frappe aérienne à venir, ils repèrent des enfants esclaves dans le complexe et tentent de faire annuler l'attaque, mais leur supérieur, Max (Jason Patric), les ignore.

L'équipe choisit d'entrer dans le complexe. Elle sauve les enfants et tue le chef du réseau dans l'action. Alors qu'un hélicoptère arrive pour les récupérer, Max, convaincu qu'ils en savent trop, ordonne de détruire l'appareil, ignorant qu'ils étaient allés sauver les enfants. Un missile détruit l'hélicoptère ainsi que les enfants. Sachant que l'attaque les visait, les Losers feignent leurs morts et se cachent en Bolivie, déterminés à prendre leur revanche sur le mystérieux Max...");
INSERT INTO post (title,author,postDate,content) VALUES ('Okja', 'Cam', '2018/02/12 18:40:02',"La société agro-alimentaire Mirando Corporation a mis au point une race de cochons génétiquement modifiés géants. Lucy Mirando (Tilda Swinton) qui dirige l’entreprise, s'étend surtout, lors d'une conférence de presse, sur les 26 premiers spécimens qui seront élevés dans la nature selon les traditions locales des différentes régions du monde où ils seront envoyés. Et dans dix ans, annonce-t-elle, une élection du plus beau cochon sera organisée.

Mija (Ahn Seo-hyeon) vit avec son grand-père dans les montagnes sud-coréennes en compagnie d'un de ces cochons, elle lui a donné le nom d'Okja. Elle ignore qu'il va lui être enlevé un jour. Quand la société Mirando vient s'en emparer, elle refuse cette séparation et se lance avec détermination dans une mission de sauvetage qui l'entraîne de l'autre coté de l'océan. Quand elle arrive en Amérique, Okja fait l'objet d'une controverse très mouvementée entre Mirando Corporation et les membres du Front de libération des animaux.");

